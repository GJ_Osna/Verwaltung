Satzung der Grünen Jugend Osnabrück
===================================

# *Präambel*
Die Grüne Jugend Osnabrück sieht sich als Organisation zur Vernetzung und Vertretung der jungen Grünen und grün-nahen Jugendlichen. Die politische Arbeit ist an den Leitbildern Ökologie, Frieden, Gleichstellung der Geschlechter, Schutz gesellschaftlicher Minderheiten, Solidarität, Antifaschismus und Antirassismus orientiert. Transparenz und Offenheit gehören zu den Grundsätzen des politischen Handelns der Grünen Jugend Osnabrück.

# §1 Name, Tätigkeitsbereich
(1) Die Organisation trägt den Namen Grüne Jugend Osnabrück. Die zu verwendende Abkürzung ist GJOS.

(2) Die GJOS ist der angegliederte Jugendverband von Bündnis 90/ Die Grünen Osnabrück und Bündnis 90/Die Grünen Osnabrück-Land, jedoch politisch und organisatorisch selbständig. Ihr Tätigkeitsbereich erstreckt sich auf die Stadt und den Landkreis Osnabrück.

# §2 Aufgaben
Die GJOS stellt sich folgende Aufgaben:

(1) Politische und organisatorische Schulungs-, Bildungs- und Informationsarbeit

(2) Zusammenarbeit mit anderen Jugendinitiativen und Interessengruppen außerhalb von Bündnis 90/Die Grünen

(3) Bündnisarbeit und Kooperationen mit anderen politischen Jugendorganisationen

(4) Vertretung der Ziele und Grundsätze der GJOS innerhalb der Jugend, der Gesellschaft und der Partei Bündnis 90/Die Grünen entsprechend den geltenden Beschlüssen

# §3 Mitgliedschaft
(1) Mitglied der GJOS kann jede natürliche Person bis zum vollendeten 28. Lebensjahr werden, die sich zu den Zielen und Grundsätzen der GJOS bekennt. Ein Mindestalter gibt es nicht. Mitglieder der Grünen Jugend Niedersachsen (GJN) aus Osnabrück und dem Landkreis Osnabrück sind Mitglieder der GJOS. Ein Widerruf ist möglich und muss gegenüber der GJN schriftlich erklärt werden. Die Mitgliedschaft in der GJOS und in einer anderen parteilichen Jugendorganisation, oder in einer faschistischen, gewaltverherrlichenden oder antidemokratischen Organisation schließen sich aus.

(2) Wird ein Mitglied der GJOS aus der GJN ausgeschlossen oder verliert anderweitig die dortige Mitgliedschaft, so ist es damit auch nicht mehr Mitglied der GJOS. Widersprüche werden ausschließlich von der GJN behandelt.

(3) Bei der GJOS kann jede:r mitarbeiten, auch ohne Mitglied der GJN zu werden, solange die in §3, Absatz 1 genannten Bedingungen erfüllt werden. Die Mitgliederversammlung (im Folgenden MV genannt) kann einzelne Nichtmitglieder von der Mitarbeit ausschließen, sofern sie sich satzungswidrig verhalten oder die Arbeit der MV ernsthaft stören oder gefährden. Mitarbeitende Menschen sind keine Mitglieder der GJN und können nicht als Schatzmeister:in oder stellvertretende:r Schatzmeister:in gewählt werden.

(4) Mitarbeitenden Menschen kann auf Antrag eines Mitglieds Stimmrecht gewährt werden. Sie haben dann den Status des kooperierenden Mitglieds. Kooperierende Mitglieder sind stimm- aber nicht wahlberechtigt. Der Status der kooperierenden Mitgliedschaft kann mit einer 2/3-Mehrheit der anwesenden Mitglieder aufgehoben werden.

(5) Die Mitgliedschaft endet durch Austritt, Ausschluss, mit dem 28. Geburtstag oder durch Tod. Über einen Ausschluss entscheidet das Schiedsgericht der Grünen Jugend Niedersachsen. 

(6) Eine altersunabhängige Fördermitgliedschaft ist möglich. Fördermitglieder sind weder wahl- noch stimmberechtigt.

# §4 Gliederung und Aufbau
(1) Die Grüne Jugend Osnabrück setzt sich aus den Einzelmitgliedern zusammen.

(2) Organe der Grünen Jugend Osnabrück sind die Mitgliederversammlung (MV), die Vollversammlung (VV) und die:der Schatzmeister:in.

(3) Alle Organe tagen grundsätzlich öffentlich. Sie können die Öffentlichkeit mit 2/3-Mehrheit ausschließen.

# §5 Vollversammlung (VV)
(1) Die VV ist das höchste beschlussfassende Gremium der Grünen Jugend Osnabrück. Sie setzt sich aus allen anwesenden Mitgliedern zusammen. Sie findet mindestens einmal im Kalenderjahr statt. Sie wird auf Beschluss der MV in geeigneter Weise unter Angabe des Tagesordnungsvorschlags mit einer Frist von mindestens 2 Wochen einberufen.

(2) Die VV
* bestimmt die Grundlagen für die politische und organisatorische Arbeit der GJOS,
* nimmt Berichte entgegen,
* beschließt über eingebrachte Anträge, wählt den Vorstand in geheimer Wahl und entlastet ihn,
* beschließt über die Satzung und über Satzungsänderungen,
* berät und beschließt den Haushalt,
* nimmt den Kassenbericht entgegen.

(3) Anträge sollten (müssen aber nicht) mindestens drei Tage vor der VV eingereicht werden, satzungsändernde Anträge müssen mindestens drei Tage vor der Vollversammlung in Textform per Mail eingereicht werden.

(4) Beschlüsse der VV sind schriftlich niederzulegen.

# §6 Mitgliederversammlung (MV)
(1) Die Mitgliederversammlung (MV) ist das zweithöchste Organ der GJOS. Sie setzt sich aus allen anwesenden Mitgliedern zusammen. Einzuberufen ist sie auf Beschluss einer MV oder einer VV oder in Sonderfällen per elektronischer Abstimmung.

(2) Die Einberufungsfrist beträgt 72 Stunden, sämtliche Mitglieder müssen in geeigneter Weise benachrichtigt werden, soweit dies mit zumutbarem Aufwand möglich ist. Dabei sind die Tagesordnung und die vorliegenden Anträge zu nennen.

(3) Sie kann über alle Anträge, die nicht die Satzung betreffen und satzungsgemäß sind, befinden und entscheiden. Die Antragstellung erfolgt gemäß §5, Absatz 3.

(4) Die MV ist für die Vergabe von Zuständigkeiten verantwortlich. Zuständigkeiten werden mit einfacher Mehrheit vergeben. Zuständige erstatten der MV regelmäßig Bericht.

(5) Die MV beschließt den Aktionsplan.

# §7 Vorstand
(1) Der Vorstand der Grünen Jugend Osnabrück besteht aus zwei Sprecher:innen, einer/einem Schatzmeister:in und einer/einem Beisitzer:in.

(2) Dem Vorstand müssen mindestens zur Hälfte FIT\*-Personen angehören, davon mindestenseine als Sprecherin.

(3) Der Vorstand vertritt die Grüne Jugend Osnabrück nach außen und zu Bündnis 90/DieGrünen.

(4) Er ist gegenüber der Mitgliederversammlung rechenschaftspflichtig.

(5) Die Wahl des Vorstands erfolgt durch die Vollversammlung gemäß §5. Die konstruktive Abwahl des Vorstandes oder einzelner Vorstandsmitglieder ist jederzeit unter Berücksichtigung von §5.1 mit Zweidrittelmehrheit möglich.

# §8 Schatzmeister:in
(1) Die:der Schatzmeister:in trägt die Verantwortung für eine ordnungsgemäße Kassenführung und die finanzielle Abrechnung. Die:der Schatzmeister:in vertritt die GJOS in allen finanziellen Angelegenheiten.

(2) Über Einzelausgaben entscheidet die MV. Zuständige können bis zu einem Betrag von 50 € in Rücksprache mit der:dem Schatzmeister:in eigenständig Zahlungen tätigen. Bei Ausgaben von mehr als 50 € hat die:der Zuständige alle Mitglieder in geeigneter Weise zu informieren und zu einer beschlussfähigen elektronischen Abstimmung mit einer Frist von 72 Stunden einzuladen.

(3) Die:der Schatzmeister:in kann und muss Ausgaben ablehnen, falls die notwendigen finanziellen Mittel nicht zur Verfügung stehen. Sie:er muss dies begründen.

(4) Die:der Schatzmeister:in legt auf der VV einen finanziellen Rechenschaftsbericht vor. Außerhalb der VV ist es die Aufgabe der Schatzmeisterin:des Schatzmeisters, allen Mitgliedern Aufschluss über die finanzielle Situation der GJOS zu geben und die Mitglieder bei der Bearbeitung von Geld betreffenden Angelegenheiten der GJOS zu unterstützen.

(5) Die:der Schatzmeister:in muss Mitglied der GJN sein.

(6) Legt die:der Schatzmeister:in das Amt nieder, so ist zum nächstmöglichen Zeitpunkt eine Nachwahl zu organisieren. Die:der neu bestimmte Schatzmeister:in wird lediglich für den Rest der ursprünglichen Amtszeit gewählt.

# §9 Allgemeine Bestimmungen
(1) Wahlen sind grundsätzlich geheim durchzuführen. Gewählt ist, wer im ersten Wahlgang die absolute Mehrheit der abgegebenen, gültigen Stimmen erhält. Enthaltungen sind gültige Stimmen. Erreicht keine:r der Bewerber:innen die erforderliche Mehrheit, so findet ein zweiter Wahlgang zwischen den beiden bestplatzierten Bewerber:innen statt, in dem die einfache Mehrheit entscheidet. Wahlen sind grundsätzlich unter Berücksichtigung des FIT\*-Statuts der GJN durchzuführen.

(2) Abstimmungen erfolgen durch Handzeichen. Auf Antrag eines Mitglieds wird eine Abstimmung geheim durchgeführt. Beschlüsse werden mit der Mehrheit der abgegebenen, gültigen Stimmen gefasst. Bei Stimmengleichheit gilt ein Antrag als abgelehnt.

(3) Diese Satzung kann nur mit einer 2/3-Mehrheit der anwesenden Mitglieder beschlossen, geändert oder aufgehoben werden, wenn dies auf der Einladung der über sie beschließenden Vollversammlung fristgerecht angekündigt wurde.

# §10 Auflösung
(1) Die Auflösung der GJOS kann nur durch eine eigens dafür einberufene VV mit Mehrheit von drei Vierteln der anwesenden Mitglieder beschlossen werden.

(2) Das Restvermögen fällt, sofern die VV nichts anderes beschließt, an Bündnis 90/Die Grünen Osnabrück und Bündnis 90/ Die Grünen Osnabrück-Land, mit der Auflage es für die Förderung der Jugend in der Partei zu verwenden.

# §11 Schlussbestimmung
Diese Satzung tritt durch Beschlussfassung der Vollversammlung in Kraft und ist ab dem 02.01.2017 gültig.
